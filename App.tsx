import React from 'react';

import { ListOfNamesConnected } from './ListOfName';
import { FetchNamesConnected } from './FetchNames';
import { API, APIContextProvider } from './AppContext';
import { SafeAreaView } from 'react-native';
import { Provider } from 'react-redux'



export default function App() {
  return (
    <APIContextProvider value={{ API }}>
      <Provider store={API.store}>
        <SafeAreaView>
          <FetchNamesConnected />
          <ListOfNamesConnected />
        </SafeAreaView>

      </Provider>
    </APIContextProvider>
  );
}
