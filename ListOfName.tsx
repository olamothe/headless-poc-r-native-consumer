import { Text, View, FlatList, SafeAreaView } from 'react-native';
import { AppState } from 'headless-ts'
import { connect } from 'react-redux'
import * as React from 'react'

export const ListOfNames: React.FunctionComponent<{ names: string[] }> = ({ names }) => {
    return (
        <SafeAreaView>
            <View>
                <FlatList
                    data={names}
                    renderItem={({ item }) => (
                        <Text style={{ color: 'black' }}>{item}</Text>
                    )}
                />
            </View>
        </SafeAreaView>
    )
}


const mapStateToProps = (state: AppState) => {
    const { names } = state

    return {
        names
    }
}

export const ListOfNamesConnected = connect(
    mapStateToProps,
)(ListOfNames)
