import * as React from 'react'
import { API as HeadlessAPI } from 'headless-ts'
export const API = new HeadlessAPI()
export const APIContext = React.createContext({ API: null })
export const APIContextProvider = APIContext.Provider
export const APIContextConsumer = APIContext.Consumer