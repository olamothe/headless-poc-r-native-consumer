import { Button } from "react-native"
import { connect } from 'react-redux'
import * as React from 'react'
import { APIContextConsumer } from "./AppContext"

export const FetchNames = () => {
    return (
        <APIContextConsumer>
            {({ API }) => (
                <Button onPress={() => API.fetchNames()} title="Fetch names" />
            )}

        </APIContextConsumer>

    )
}


export const FetchNamesConnected = connect(null, null)(FetchNames)